from itertools import product
from Crypto.Hash import keccak

alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()+'

hashes = {'a4bb6b2fe56f94056b2e49734611be1d69a5c352be0e5980a1b58396f8f96cd4', '0cd68db43edc96a8fe178d4fe4397ef4bd45f66fac9e9e9d4ab34e34c86b4686'}

pass_length = 0

while len(hashes) > 0:
    pass_length += 1

    combinations = product(alphabet, repeat=pass_length)
    
    for combination in combinations:
        password = ''.join(combination)
        keccak_hash = keccak.new(digest_bits=256)
        keccak_hash.update(password.encode('utf-8'))
        hash = keccak_hash.hexdigest()

        if hash in hashes:
            print(f'password = {password}, hash = {hash}')
            hashes.remove(hash)

        if len(hashes) == 0:
            break
