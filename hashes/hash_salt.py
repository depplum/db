from itertools import product
from Crypto.Hash import keccak

alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()+'

hash = '2817bd26c3119c393668218d33288b5eb017a0fa154fc755782f08a1597521e6'
salt = '363bc6f8-0362-47fa-8fec-58726d940b47'

found = False
pass_length = 0

while not found:
    pass_length += 1

    combinations = product(alphabet, repeat=pass_length)
    
    for combination in combinations:
        password = ''.join(combination)

        keccak_hash = keccak.new(digest_bits=256)
        keccak_hash.update((password + salt).encode('utf-8'))
        hash = keccak_hash.hexdigest()
        if hash == hash:
            print(f'password = {password}')
            found = True
            break
