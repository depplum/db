import json, logging, os, psycopg2
from flask import Flask, request
from psycopg2.extras import Json, RealDictCursor
from redis import Redis



app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False

def get_pg_connection():
    pg_conn = psycopg2.connect(host=os.getenv('POSTGRES_HOST') or '127.0.0.0.1', port=os.getenv('POSTGRES_PORT'),
                               database=os.getenv('POSTGRES_DB'), user=os.getenv('POSTGRES_USER'), 
                               password=os.getenv('POSTGRES_PASSWORD'), cursor_factory=RealDictCursor)
    pg_conn.autocommit = True
    return pg_conn

def get_redis_connection():
    return Redis(host=os.getenv('REDIS_HOST') or '127.0.0.1', port=os.getenv('REDIS_PORT'), password=os.getenv('REDIS_PASSWORD'), decode_responses=True)


@app.route('/actors')
def get_actors():
    try:
        offset = request.args.get('offset')
        limit = request.args.get('limit')
        redis_key = f'actors:offset={offset},limit={limit}'
        with get_redis_connection() as redis_conn:
            redis_actors = redis_conn.get(redis_key)

        if redis_actors is None:
            query = """
            select a.id as actor_id, a.actor_id as actor_name, a.birth_date as actor_birth,
            marital_status as actor_marital, zodiac_sign as actor_zodiac, m.id as movie_id, 
            m.movie_id as movie_name, m.movie_release  as movie_release, director as movie_director, duration as movie_duration
            from actor as a
            left join actor_to_movie on a.id = actor_to_movie.actor_id 
            left join movie as m on actor_to_movie.movie_id = m.id 
            offset %s
            limit %s
            """
            with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
                cur.execute(query, (offset, limit))
                rows = cur.fetchall()
            
            redis_actors = json.dumps(rows, ensure_ascii=False, default=str, indent=2)
            
            with get_redis_connection() as redis_conn:
                 redis_conn.set(redis_key, redis_actors, ex=30)
        
        return redis_actors, 200, {'content-type': 'text/json'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400

@app.route('/actors/create', methods=['POST'])
def create_actor():
    try:
        body = request.json
        actor_id = body['actor_id']
        birth_date = body['birth_date']
        marital_status = body['marital_status']
        zodiac_sign = body['zodiac_sign']
    
        query = f"""
        insert into actor (actor_id, birth_date, marital_status, zodiac_sign)
        values (%s, %s, %s, %s)
        returning actor_id, birth_date, marital_status, zodiac_sign
        """

        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query, (actor_id, birth_date, marital_status, zodiac_sign))
            rows = cur.fetchall()

        return {'message': f'Actor {rows[0]["actor_id"]} with birth date {rows[0]["birth_date"]} with created.'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


@app.route('/actors/update', methods=['POST'])
def update_actor():
    try:
        body = request.json
        id = body['id']
        actor_id = body['actor_id']
        
        query = f"""
        update actor
        set actor_id = %s
        where id = %s
        returning id
        """
        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query, (actor_id, id))
            affected_rows = cur.fetchall()
        cur.close()
        if len(affected_rows):
            return {'message': f'Actor with id = {id} updated.'}
        else:
            return {'message': f'Actor with id = {id} not found.'}, 404
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


@app.route('/actors/delete', methods=['DELETE'])
def delete_actor():
    try:
        body = request.json
        id = body['id']
        
        query = f"""
        delete from actor
        where id = %s
        returning id
        """
        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query, (id,))
            affected_rows = cur.fetchall()
        
        if len(affected_rows):
            return {'message': f'Actor with id = {id} deleted.'}
        else:
            return {'message': f'Actor with id = {id} not found.'}, 404
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


@app.route('/index', methods=['POST'])
def index():
    try:
        body = request.json
        name = body['name']
        year = body['year']
        reward_year = body['reward_year']
        query = f"""
        select *
        from reward
        where name = %s and year = %s and reward_year = %s;
        """

        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query, (name, year, reward_year))
            rows = cur.fetchall()
        
        return {'message': f'Reward {rows[0]} existed.'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400

@app.route('/materialis')
def mat_eria():
    try:
        
        query = """
    select *
    from actor_and_movie;"""

        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query)
            graph = cur.fetchall()
            
        redis_graph = json.dumps(graph, default=vars, ensure_ascii=False, indent=2)
            
        return redis_graph, 200, {'content-type': 'text/json'}

    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400

@app.route('/matin', methods=['GET'])
def get_mat_index():
    try:
        body = request.json
        actor_id = body['actor_id']
        movie_name = body['movie_name']
        movie_release = body['movie_release']
        query = f"""
        select *
        from actor_and_movie
        where actor_id = %s and movie_name = %s and movie_release >= %s ;
        """
        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
                cur.execute(query, (actor_id, movie_name, movie_release))
                
                rows = cur.fetchall()

        
        return {'message': f'{rows[0]}'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400

# # if __name__ == '__main__':
# #     app.run(port=8000, host='127.0.0.1')


@app.route('/actor/jsonb', methods =['POST'])
def json_db():
    try:
        body = request.json
        name = body['name']
        query_param = [{"name": name}]

        query=f"""
            select *
            from actor
            where education @> %s;
            """

        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
                cur.execute(query, (json.dumps(query_param),))
                rows = cur.fetchall()

        return {'message': f'{rows[0]}'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


@app.route('/actor/array', methods =['POST'])
def array_db():
    try:
        body = request.json
        languange = body['languange']

        query=f"""
            select *
            from movie
            where languange && array [%s];
            """

        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
                cur.execute(query, (languange,))
                rows = cur.fetchall()

        return {'message': f'{rows[0]}'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400






