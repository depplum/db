alter table actor 
	add column education jsonb;

UPDATE actor 
    SET education =  (case when zodiac_sign = 'gemini' then '[{
                                            "name": "СПбГУ",
                                            "completion_date": 2015,
                                            "degree": "bachelor"
                                            },
                                            {
                                            "name": "МГУ",
                                            "completion_date": 2020,
                                            "degree": "magister"
                                            }]'::jsonb

                            when zodiac_sign = 'aries' then '[{
                                            "name": "МГИМО",
                                            "completion_date": 2005,
                                            "degree": "bachelor"
                                            }]'::jsonb
                            when zodiac_sign = 'capricorn' then '[{
                                            "name": "МСУ",
                                            "completion_date": 2018,
                                            "degree": "bachelor"
                                            },
                                            {
                                            "name": "АГУ",
                                            "completion_date": 2023,
                                            "degree": "magister"
                                            }]'::jsonb
                            when zodiac_sign = 'scorpio' then '[{

                                            "name": "СИРИУС",
                                            "completion_date": 2000,
                                            "degree": "bachelor"
                                            }]'::jsonb
                                    end);



-- добавление индекса
create index actor_education on actor using gin (education);


--explain
--select *
--from actor
--where education @> '[{"name": "МГУ"}]';
