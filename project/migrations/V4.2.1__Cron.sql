create extension pg_cron;

-- refresh представления каждую минуту:
select cron.schedule('actor_and_movie', '* * * * *',
                     $$ refresh materialized view concurrently actor_and_movie $$);

-- удаление задачи:
-- select cron.unschedule('actor_and_movie');