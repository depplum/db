drop function if exists refresh_actor_and_movie;
create function refresh_actor_and_movie()
        returns trigger as
$$
begin
    refresh materialized view concurrently actor_and_movie;

    return new;
end;
$$
    language 'plpgsql';

create trigger update_movie_table
    after insert or update or delete
    on movie
    for each row
execute function refresh_actor_and_movie();

