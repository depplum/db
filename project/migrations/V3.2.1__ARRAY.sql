alter table movie add column languange text[];

UPDATE movie
    SET languange = (case when movie_release = 2007 then '{russian, english}'::text[]
                         when movie_release = 2016 then '{english}'::text[]
                         when movie_release = 2003 then '{english, spanish}'::text[]
                         when movie_release = 2014 then '{russian}'::text[]
                         when movie_release = 2019 then '{germany}'::text[]
                    end);
        
create index movie_languange on movie using gin (languange);


