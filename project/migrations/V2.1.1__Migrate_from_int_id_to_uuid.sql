begin;

create extension if not exists "uuid-ossp";

-- 1) из таблицы, которая ссылаются на actor, удаляем constraint с внешним ключом
alter table actor_to_movie
    drop constraint actor_to_movie_actor_id_fkey;

-- 2) переименовываем колонку со старым внешним ключом
alter table actor_to_movie
    rename column actor_id to old_actor_id;

-- 3) добавляем колонку с новым внешним ключом
alter table actor_to_movie
    add column actor_id uuid;


-- проделываем пункты 1-2-3 для всех таблиц, которые ссылаются на actor
alter table reward
    drop constraint reward_actor_id_fkey;

alter table reward
    rename column actor_id to old_actor_id;

alter table reward
    add column actor_id uuid;


-- в таблице actor удаляем constraint primary key
alter table actor
    drop constraint actor_pkey;


-- переименовываем колонку со старым ключом
alter table actor
    rename column id to old_id;


-- добавляем колонку с новым ключом
alter table actor
    add column id uuid default uuid_generate_v4();

/*
анонимная функция, которая циклом обходит все строки таблицы actor,
для каждой строки добавляет новый id, и всем ссылающимся на эту строку
строкам из других таблиц проставляет новый id
*/
do
$$
    declare
        row record;
    begin
        for row in select * from actor
            loop
                update actor_to_movie set actor_id = row.id where old_actor_id = row.old_id;
                update reward set actor_id = row.id where old_actor_id = row.old_id;
            end loop;
    end
$$;

drop materialized view if exists actor_and_movie cascade;


-- удаляем столбец со старым id
alter table actor
    drop column old_id;

-- устанавливаем первичный ключ
alter table actor
    add primary key (id);


-- удаляем столбец со старым внешним ключом
-- удаление этой колонки так же удалит установленное ранее ограничение на уникальность
alter table actor_to_movie
    drop column old_actor_id;

-- устанавливаем новый внешний ключ
alter table actor_to_movie
    add constraint fk_actor_id foreign key (actor_id) references actor;

-- для связи many-to-many ставим соответствующие ограничения
alter table actor_to_movie
    alter column actor_id set not null;
alter table actor_to_movie
    add constraint uq_actor_id_to_movie_id unique (actor_id, movie_id);


-- удаляем столбец со старым внешним ключом
alter table reward
    drop column old_actor_id;
        
-- устанавливаем новый внешний ключ   
alter table reward
    add constraint fk_actor_id foreign key (actor_id) references actor;







create materialized view actor_and_movie as
select a.id as actor_id, a.actor_id as actor_name, a.birth_date as actor_birth,
            m.id as movie_id, m.movie_id as movie_name, m.movie_release  as movie_release
from actor as a
            left join actor_to_movie on a.id = actor_to_movie.actor_id 
            left join movie as m on actor_to_movie.movie_id = m.id;

drop index if exists actor_and_movie_id;

create unique index actor_and_movie_id on actor_and_movie (actor_id, movie_id);

refresh materialized view concurrently actor_and_movie;

drop index if exists actor_and_movie_search;
create index actor_and_movie_search on actor_and_movie
    using btree (actor_birth, movie_name, movie_release);

alter table actor_to_movie add primary key(actor_id, movie_id);

-- завершение транзакции
commit;
