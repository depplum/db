drop table if exists actor, movie, reward, actor_to_movie cascade;
drop table if exists movie cascade;
drop table if exists reward cascade;
drop table if exists actor_to_movie cascade;

create table actor
(
    id int generated always as identity primary key,
    actor_id text not null,
    birth_date text,
    marital_status text,
    zodiac_sign text
);

create table movie
(
    id bigint generated always as identity primary key,
    movie_id text,
    movie_release int,
    director text,
    duration int
);

create table reward
(
    id int generated always as identity primary key,
    name text,
    actor_id int references actor not null,
    reward_year text,
    year int
);

create table actor_to_movie
(
    actor_id int references actor not null,
    movie_id int references movie not null,
    primary key (actor_id, movie_id)
);

insert into actor (actor_id, birth_date, marital_status, zodiac_sign)
values ('Johnny Depp', '09/06/1963','divorced', 'gemini'),
       ('Keira Knightley', '26/03/1985', 'married', 'aries'),
       ('Timothy Chalamet','27/12/1995','single', 'capricorn'),
       ('Anne Hathaway', '12/11/1982', 'single', 'scorpio');

insert into movie (movie_id, movie_release, director, duration)
values ('Redemption', 2007, 'Stephen Knight', 1.57),
       ('Split', 2016, 'Manoj Nelliyattu', 1.57),
       ('Pirates of the Caribean', 2003, 'Espen Sandberg', 2.23),
       ('Interstellar', 2014, 'Christopher Nolan', 2.49),
       ('The little women', 2019, 'Greta Gervig', 2.15);

insert into reward (name, actor_id, reward_year, year)
values ('Oscar', 1, '1929/2022', 2002),
       ('Golden globe', 1, '1944/2022', 2015),
       ('Cesar', 3, '1976/2022', 2007),
       ('People`s Choice Awards', 4, '1975/2022', 2019),
       ('Critics Choice', 4, '1995/2022', 2010),
       ('Gotham', 2, '1991/2022', 2015);

insert into actor_to_movie (actor_id, movie_id)
values (1, 3),
       (1, 4),
       (1, 5),
       (2, 1),
       (2, 4),
       (3, 3),
       (3, 5),
       (4, 1),
       (4, 4);




create materialized view actor_and_movie as
select a.id as actor_id, a.actor_id as actor_name, a.birth_date as actor_birth,
            m.id as movie_id, m.movie_id as movie_name, m.movie_release  as movie_release
from actor as a
            left join actor_to_movie on a.id = actor_to_movie.actor_id 
            left join movie as m on actor_to_movie.movie_id = m.id;

-- select *
-- from actor_and_movie;

drop index if exists actor_and_movie_id;

create unique index actor_and_movie_id on actor_and_movie (actor_id, movie_id);

refresh materialized view concurrently actor_and_movie;




drop index if exists reward_act_name_year;
create index reward_act_name_year on reward
    using btree (name, year, reward_year);

-- select *
-- from reward
-- where name = 'Oscar';



drop index if exists actor_and_movie_search;
create index actor_and_movie_search on actor_and_movie
    using btree (actor_birth, movie_name, movie_release);

-- select *
-- from actor_and_movie
-- where actor_id <= 2 and movie_id >= 3;
